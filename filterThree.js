const data = require("./data.js");

// Import function error handler
const errorHandler = require("./errorHandler.js");

function filterData3 (data) {
// Tempat penampungan hasil
const result = [];

data.forEach((data) => {
    if (data.eyeColor === "blue" && data.age >= 35 && data.age <= 40 && data.favoriteFruit === "apple") {
        result.push(data);
    }
   
})

return (result.length < 1) ? errorHandler() : result;
}

module.exports = filterData3(data)