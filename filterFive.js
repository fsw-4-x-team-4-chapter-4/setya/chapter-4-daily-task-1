const data = require("./data.js");

// Import function error handler
const errorHandler = require("./errorHandler.js");

function filterData5 (data) {
// Tempat penampungan hasil
const result = [];

data.forEach((data) => {
    if (data.registered < "2016-10-11T12:25:56 -07:00" && data.isActive) {
        result.push(data);
    }
    
})

return (result.length < 1) ? errorHandler() : result;
}

module.exports = filterData5(data)