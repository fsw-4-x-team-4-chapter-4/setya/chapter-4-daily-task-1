const data = require("./data.js");

// Import function error handler
const errorHandler = require("./errorHandler.js");

function filterData1 (data) {
// Tempat penampungan hasil
const result = [];

data.forEach((data) => {
    if (data.age < 30 && data.favoriteFruit === "banana") {
        result.push(data);
    }
    
})

return (result.length < 1) ? errorHandler() : result;
}

module.exports = filterData1(data)